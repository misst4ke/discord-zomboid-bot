"""Discord client"""
import datetime
import logging.config

import discord
from discord.ext import tasks

import cmd
from settings import settings, _

utc = datetime.timezone.utc

logging.config.fileConfig('logging.conf', disable_existing_loggers=False)
logger = logging.getLogger(__name__)

intents = discord.Intents.default()
intents.message_content = True


# scheduled task to update mods to the server
# UTC
TIMES = [datetime.time(hour=4, minute=30)]


class MyClient(discord.Client):
    async def on_ready(self):
        self.cron.start()

    @tasks.loop(time=TIMES)
    async def cron(self):
        disconnect_messages = [
            dict(minutes=5, message=_('auto-reboot-of-server-in-n-minutes')),
            dict(minutes=2, message=_('reboot-of-server-in-n-minutes')),
            dict(minutes=1, message=_('reboot-of-server-in-n-minutes')),
            dict(minutes=1, message=_('reboot-of-server-in-n-minutes')),
            dict(seconds=30, message=_('reboot-of-server-in-n-minutes')),
            dict(seconds=20, message=_('reboot-of-server-in-n-minutes')),
            dict(seconds=5, message=_('reboot-now')),
            dict(seconds=5, message=_('reboot-now')),
        ]

        logger.info(_('reboot-program-task'))
        await cmd.disconnect_msg(disconnect_messages)

        logger.info(_('message-send-save-and-reboot-server'))
        await cmd.stop_server()

        logger.info(_('server-saved-and-shutdown.'))
        await cmd.update_server()

        logger.info(_('server-updated'))
        cmd.start_server()

        logger.info(_('server-rebooted'))

    async def on_message(self, message):  # noqa
        disconnect_messages = [
            dict(minutes=2, message=_('reboot-of-server-in-n-minutes')),
            dict(minutes=1, message=_('reboot-of-server-in-n-minutes')),
            dict(minutes=1, message=_('reboot-of-server-in-n-minutes')),
            dict(seconds=30, message=_('reboot-of-server-in-n-seconds')),
            dict(seconds=20, message=_('reboot-of-server-in-n-seconds')),
            dict(seconds=5, message=_('reboot-now')),
            dict(seconds=5, message=_('reboot-now')),
        ]

        if message.content.startswith(settings.discord_root_command):  # noqa
            if settings.discord_command_help in message.content:
                await message.channel.send('los comandos disponibles son')
                for com in [settings.discord_command_restart_server, settings.discord_command_our_mods]:
                    await message.channel.send(f'- {settings.discord_root_command} {com}')

            if settings.discord_command_restart_server in message.content:
                logger.info('Solicitado reinicio del servidor.')

                await cmd.disconnect_msg(disconnect_messages)

                logger.info(_('message-send-save-and-reboot-server'))
                await cmd.stop_server()

                logger.info(_('server-saved-and-shutdown.'))
                await cmd.update_server()

                logger.info(_('server-updated'))
                cmd.start_server()

                logger.info(_('server-rebooted'))
                await message.channel.send(_('reboot-completed'))

            if settings.discord_command_our_mods in message.content:
                for mod in settings.url_mods:
                    await message.channel.send(mod)


MyClient(intents=intents).run(token=settings.discord_secret_key)
