import asyncio
import subprocess
import logging
from typing import Union

from settings import settings, _

logger = logging.getLogger(__name__)


def execute_cmd(cmd: str):
    subprocess.run(cmd, stdout=subprocess.PIPE, shell=True)


async def sleep(minutes: int = 0, seconds: int = 0):
    """Wait minutes or seconds.

    sleep not wait by default

    :raise Exception: when define minutes AND seconds at same time
    """
    if minutes and seconds:
        raise Exception(_('raise-when-minutes-and-seconds-is-not-in-params'))
    if minutes:
        seconds = minutes * 60
    await asyncio.sleep(seconds)


async def disconnect_msg(disconnect_messages: list[dict[str, Union[int, str]]]):
    """Get messages to the server world

    :param disconnect_messages: list of dicts with minutes:int or seconds:int and message:str
    """

    if disconnect_messages is None:
        # default value
        disconnect_messages = [dict(minutes=5, message=_('default-reboot-of-server-in-n-minutes'))]

    remainder: int = sum(x.get('seconds', 0) for x in disconnect_messages)
    remainder += sum(x.get('minutes', 0) * 60 for x in disconnect_messages)

    for disconnect_message in disconnect_messages:
        seconds: int = disconnect_message.get('seconds')
        minutes: int = disconnect_message.get('minutes')
        message: str = disconnect_message.get('message', _('default-reboot-of-server-in-n'))  # default value
        if seconds:
            msg: str = message.format(time=remainder)
            remainder -= seconds
            execute_cmd(settings.cmd_alert.format(msg=msg))
            logger.info(msg)
            await sleep(seconds=seconds)
        elif minutes:
            msg: str = message.format(time=round(remainder / 60))
            remainder -= minutes * 60
            execute_cmd(settings.cmd_alert.format(msg=msg))
            logger.info(msg)
            await sleep(minutes=minutes)
        else:
            raise Exception(_('raise-when-minutes-and-seconds-is-not-in-params'))


def start_server():
    """Start server"""
    execute_cmd(settings.cmd_start)


async def stop_server():
    """Save and stop server with delays"""
    execute_cmd(settings.cmd_save)
    await sleep(seconds=settings.sleep_after_save)
    execute_cmd(settings.cmd_stop)
    await sleep(seconds=settings.sleep_after_stop)


async def update_server():
    """Update server with delays"""
    execute_cmd(settings.cmd_update)
    await sleep(seconds=settings.sleep_after_update)
