import json
from functools import lru_cache

from pydantic import BaseSettings, HttpUrl


class _Settings(BaseSettings):
    app_name: str

    # .secrets
    discord_secret_key: str
    discord_report_channel_id: int

    # application app language
    default_lang: str = 'es'
    supported_langs: list[str] = ['es']

    # in seconds
    sleep_after_save: int = 5
    sleep_after_stop: int = 30
    sleep_after_update: int = 60

    # commands executed directly on "project zomboid" server
    cmd_alert: str
    cmd_start: str
    cmd_save: str
    cmd_stop: str
    cmd_update: str

    # commands for discord server. Syntax: `<DISCORD_ROOT_COMMAND> <DISCORD_COMMAND_HELP_*>`
    discord_root_command: str
    discord_command_help: str
    discord_command_restart_server: str
    discord_command_our_mods: str

    # Mods installed on the server, you can queryed from discord
    # with `<DISCORD_ROOT_COMMAND> <DISCORD_COMMAND_OUR_MODS>`
    url_mods: list[HttpUrl] = [
        "https://steamcommunity.com/sharedfiles/filedetails/?id=2335368829",
        "https://steamcommunity.com/sharedfiles/filedetails/?id=2282429356",
        "https://steamcommunity.com/sharedfiles/filedetails/?id=2592358528",
        "https://steamcommunity.com/sharedfiles/filedetails/?id=2808679062",
        "https://steamcommunity.com/sharedfiles/filedetails/?id=2313387159",
        "https://steamcommunity.com/sharedfiles/filedetails/?id=2594865484",
        "https://steamcommunity.com/sharedfiles/filedetails/?id=2460154811",
        "https://steamcommunity.com/sharedfiles/filedetails/?id=2200148440",
        "https://steamcommunity.com/sharedfiles/filedetails/?id=2423906082",
        "https://steamcommunity.com/sharedfiles/filedetails/?id=2690908199",
        "https://steamcommunity.com/sharedfiles/filedetails/?id=2875848298",
        "https://steamcommunity.com/sharedfiles/filedetails/?id=2186592938",
        "https://steamcommunity.com/sharedfiles/filedetails/?id=2529746725",
        "https://steamcommunity.com/sharedfiles/filedetails/?id=2458631365",
        "https://steamcommunity.com/sharedfiles/filedetails/?id=2447729538",
        "https://steamcommunity.com/sharedfiles/filedetails/?id=2616986064",
        "https://steamcommunity.com/sharedfiles/filedetails/?id=2402057349",
        "https://steamcommunity.com/sharedfiles/filedetails/?id=2544353492",
        "https://steamcommunity.com/sharedfiles/filedetails/?id=2714198296",
        "https://steamcommunity.com/sharedfiles/filedetails/?id=1969456967",
        "https://steamcommunity.com/sharedfiles/filedetails/?id=566115016",
        "https://steamcommunity.com/sharedfiles/filedetails/?id=2650547917",
        "https://steamcommunity.com/sharedfiles/filedetails/?id=2710167561",
        "https://steamcommunity.com/sharedfiles/filedetails/?id=2734705913",
        "https://steamcommunity.com/sharedfiles/filedetails/?id=2004998206",
        "https://steamcommunity.com/sharedfiles/filedetails/?id=2694448564",
        "https://steamcommunity.com/sharedfiles/filedetails/?id=2169435993",
        "https://steamcommunity.com/sharedfiles/filedetails/?id=2685168362",
        "https://steamcommunity.com/sharedfiles/filedetails/?id=2659216714",
        "https://steamcommunity.com/sharedfiles/filedetails/?id=2956146279",
        "https://steamcommunity.com/sharedfiles/filedetails/?id=2657661246",
        "https://steamcommunity.com/sharedfiles/filedetails/?id=2377867605",
        "https://steamcommunity.com/sharedfiles/filedetails/?id=2142622992",
        "https://steamcommunity.com/sharedfiles/filedetails/?id=2142622992",
        "https://steamcommunity.com/sharedfiles/filedetails/?id=2463184726",
        "https://steamcommunity.com/sharedfiles/filedetails/?id=2680473910",
        "https://steamcommunity.com/sharedfiles/filedetails/?id=2487022075",
        "https://steamcommunity.com/sharedfiles/filedetails/?id=2648779556",
        "https://steamcommunity.com/sharedfiles/filedetails/?id=2392709985",
        "https://steamcommunity.com/sharedfiles/filedetails/?id=2954422590",
        "https://steamcommunity.com/sharedfiles/filedetails/?id=2687798127",
        "https://steamcommunity.com/sharedfiles/filedetails/?id=2619072426",
        "https://steamcommunity.com/sharedfiles/filedetails/?id=2686624983",
    ]

    class Config:
        env_file = '.env', '.secrets'


settings: _Settings = _Settings()


@lru_cache(maxsize=500)
def _(key: str) -> str:
    if settings.default_lang not in settings.supported_langs:
        return key
    with open('i18n.json') as f:
        i18n = json.load(f)
    return i18n.get(key, {}).get(settings.default_lang, key)
