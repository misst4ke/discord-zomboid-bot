# discord-zomboid-bot


## Instructions

* You need create a bot on [discord](https://discord.com/developers/applications/), follow the discord guides.
* Create a .secret file and adds `DISCORD_SECRET_KEY` of bot and `DISCORD_REPORT_CHANNEL_ID` the channel of your server.
* You can add languages or change the messages editing [i18n.json](./src/i18n.json), `LANG` and `SUPPORTED_LANGS` of [.env](./src/.env) file.
* For cron update you can edit `TIMES` on [main.py](./src/main.py).
* For delayed messages you can edit `disconnect_messages`  on [main.py](./src/main.py).